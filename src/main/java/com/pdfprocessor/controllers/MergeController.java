package com.pdfprocessor.controllers;


import com.pdfprocessor.dtos.FileDto;
import com.pdfprocessor.dtos.Signature;
import com.pdfprocessor.services.PdfMergeService;
import com.pdfprocessor.utils.Filename;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin(origins = "*")
public class MergeController {

    @Autowired
    private PdfMergeService mergerService;


    @PostMapping(
            value = "/merger",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FileDto> merge(
            @RequestPart("pdf-files")MultipartFile [] multipartFiles,
            @RequestParam("file-name")String filename,
            @RequestParam String title,
            @RequestParam String creator,
            @RequestParam String subject
    ) {

        String originalFilename = Filename.pdfFilename(filename);

        FileDto fileDto = new FileDto(
                originalFilename,
                mergerService.merge(
                        multipartFiles,
                        filename,
                        new Signature(title, creator, subject)
                ).getFileName().toString());

        return ResponseEntity.ok().body(fileDto);


    }


}
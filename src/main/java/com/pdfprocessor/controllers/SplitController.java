package com.pdfprocessor.controllers;

import com.pdfprocessor.dtos.FileDto;
import com.pdfprocessor.services.PdfSplitService;
import com.pdfprocessor.utils.Filename;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin(origins = "*")

public class SplitController {


    @Autowired
    private PdfSplitService splitterService;


    @PostMapping(
            value = "/splitter",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FileDto> split(
            @RequestPart("pdf-file") MultipartFile multipartFile,
            @RequestParam("file-name") String filename,
            @RequestParam("pages") int pages
    )  {

        String originalFilename = Filename.zipFilename(filename);

        FileDto fileDto;
        fileDto = new FileDto(
                originalFilename,
                splitterService.split(
                        multipartFile,
                        filename,
                        pages
                ).getFileName().toString()
        );

        return ResponseEntity.ok().body(fileDto);

    }




}
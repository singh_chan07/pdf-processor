package com.pdfprocessor.controllers;


import com.pdfprocessor.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class DownloadController {


    @Autowired
    private FileService fileService;


    @GetMapping(
            value = "/downloader/{file-uri}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public ResponseEntity<byte[]> download(
            @PathVariable("file-uri") String fileUri) {


        String filename = fileService.getFilename(fileUri);

        byte [] data = fileService.fetch(fileUri);
        fileService.delete(fileUri);

        return  ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
                .body(data);

    }
}
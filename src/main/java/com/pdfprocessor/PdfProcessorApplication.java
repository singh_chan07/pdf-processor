package com.pdfprocessor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@Slf4j
public class PdfProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfProcessorApplication.class, args);
	}

}

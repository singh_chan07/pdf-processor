package com.pdfprocessor.services;


import com.pdfprocessor.core.Merger;
import com.pdfprocessor.dtos.Signature;
import com.pdfprocessor.exceptions.FileNotSupportedException;
import com.pdfprocessor.exceptions.GeneralException;
import com.pdfprocessor.utils.Filename;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.xmpbox.XMPMetadata;
import org.apache.xmpbox.schema.DublinCoreSchema;
import org.apache.xmpbox.schema.PDFAIdentificationSchema;
import org.apache.xmpbox.schema.XMPBasicSchema;
import org.apache.xmpbox.type.BadFieldValueException;
import org.apache.xmpbox.xml.XmpSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.transform.TransformerException;
import java.io.*;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;


/**
 * PdfMergeService creates a file where output will be stored, then creates a list of input streams and
 * pass that to pdfmergerutility class of pdf-box which returns a outputstream, then the path of the output file
 * is returned.
 * Private methods other than close everything are used for uniform signature oll over the final document
 */
@Service
public class PdfMergeServiceImpl implements PdfMergeService {


    @Autowired
    private FileServiceImpl fileService;

    @Override
    public Path merge(MultipartFile[] multipartFiles, String filename, Signature signature){



        COSStream cosStream = new COSStream();

        Path outputPath = fileService.createFile(Filename.pdfFilename(filename));

        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(outputPath.toFile());
        } catch (FileNotFoundException e) {
            throw new GeneralException("Internal Server Error", e);
        }

        List<InputStream> inputStreamList = new LinkedList<>();

        for(MultipartFile multipartFile: multipartFiles){

            try{
                inputStreamList.add(multipartFile.getInputStream());
            }catch (IOException ioException){
                closeEverything(null, fileOutputStream, cosStream, outputPath);
                throw new FileNotSupportedException("One of the File is corrupt or not PDF/A-1b compliant.");
            }

        }
        PDDocumentInformation pdDocumentInformation = createPDFDocumentInfo(signature);

        PDMetadata xmpMetadata;
        try {
            xmpMetadata = createXMPMetadata(cosStream, signature);
        } catch (BadFieldValueException | TransformerException | IOException e) {
            closeEverything(null, fileOutputStream, cosStream, outputPath);
            throw new GeneralException("Internal Server Error!!!", e);
        }

        try {
            Merger.merge(inputStreamList, pdDocumentInformation, xmpMetadata, fileOutputStream);
        } catch (IOException ioException) {
            closeEverything(null, fileOutputStream, cosStream, outputPath);
            throw new FileNotSupportedException("One of the File is corrupt or not PDF/A-1b compliant.");
        }

        closeEverything(null, fileOutputStream, cosStream, null);
        return outputPath;

    }


    private void closeEverything(FileInputStream fileInputStream, FileOutputStream fileOutputStream, COSStream cosStream, Path path){

        try{
            if(fileInputStream!=null){
                fileInputStream.close();
            }
            if(fileOutputStream!=null){
                fileOutputStream.close();
            }
            if(cosStream!=null){
                cosStream.close();
            }
            if (path!=null){
                fileService.delete(path.getFileName().toString());
            }
        }catch (IOException ioException){
            throw new GeneralException("Internal Server Error", ioException);
        }


    }

    private PDDocumentInformation createPDFDocumentInfo(Signature signature)
    {

        PDDocumentInformation documentInformation = new PDDocumentInformation();
        documentInformation.setTitle(signature.getTitle());
        documentInformation.setCreator(signature.getCreator());
        documentInformation.setSubject(signature.getSubject());
        return documentInformation;
    }

    private PDMetadata createXMPMetadata(COSStream cosStream, Signature signature)
            throws BadFieldValueException, TransformerException, IOException
    {

        XMPMetadata xmpMetadata = XMPMetadata.createXMPMetadata();

        // PDF/A-1b properties
        PDFAIdentificationSchema pdfaSchema = xmpMetadata.createAndAddPFAIdentificationSchema();
        pdfaSchema.setPart(1);
        pdfaSchema.setConformance("B");

        // Dublin Core properties
        DublinCoreSchema dublinCoreSchema = xmpMetadata.createAndAddDublinCoreSchema();
        dublinCoreSchema.setTitle(signature.getTitle());
        dublinCoreSchema.addCreator(signature.getTitle());
        dublinCoreSchema.setDescription(signature.getSubject());

        // XMP Basic properties
        XMPBasicSchema basicSchema = xmpMetadata.createAndAddXMPBasicSchema();
        Calendar creationDate = Calendar.getInstance();
        basicSchema.setCreateDate(creationDate);
        basicSchema.setModifyDate(creationDate);
        basicSchema.setMetadataDate(creationDate);
        basicSchema.setCreatorTool(signature.getCreator());

        // Create and return XMP data structure in XML format
        try (ByteArrayOutputStream xmpOutputStream = new ByteArrayOutputStream();
             OutputStream cosXMPStream = cosStream.createOutputStream())
        {
            new XmpSerializer().serialize(xmpMetadata, xmpOutputStream, true);
            cosXMPStream.write(xmpOutputStream.toByteArray());
            return new PDMetadata(cosStream);
        }
    }






}
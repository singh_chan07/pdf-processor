package com.pdfprocessor.services;


import com.pdfprocessor.dtos.Signature;
import org.apache.xmpbox.type.BadFieldValueException;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.nio.file.Path;

public interface PdfMergeService {

    Path merge(MultipartFile [] multipartFiles, String filename, Signature signature);
}

package com.pdfprocessor.services;

import com.pdfprocessor.configurations.DirectoryConfigProperty;
import com.pdfprocessor.exceptions.GeneralException;
import com.pdfprocessor.utils.PathRegistry;
import org.apache.pdfbox.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
@EnableConfigurationProperties(DirectoryConfigProperty.class)
public class FileServiceImpl implements FileService{

    @Autowired
    private PathRegistry pathRegistry;

    @Autowired
    DirectoryConfigProperty directory;

    @Override
    public Path store(MultipartFile multipartFile, String originalName)  {

        Path path = createFile(originalName);

        try{
            Files.copy(multipartFile.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException ie){
            throw new GeneralException("Internal Server error", ie);
        }

        return path;
    }


    @Override
    public Path createFile(){
        Path path = pathRegistry.generatePath();

        return path;
    }

    @Override
    public Path createFile(String originalName) {
        return pathRegistry.generatePath(originalName);
    }


    @Override
    public void delete(String fileNumber) {
        Path path = Paths.get(directory.getDirectory(), fileNumber);
        try{
            Files.delete(path);
            pathRegistry.deletePath(path);
        }catch (IOException ioException){
            throw new GeneralException("Internal Server Error: ", ioException);
        }
    }

    @Override
    public String getFilename(String fileNumber){
        return pathRegistry.getFilename(fileNumber);
    }

    @Override
    public byte[] fetch(String fileNumber) {
        Path path = Paths.get(directory.getDirectory(), fileNumber);
        FileInputStream inputStream;
        byte [] data;

        try {
            inputStream = new FileInputStream(path.toFile());
            data = IOUtils.toByteArray(inputStream);
            inputStream.close();
        }catch (IOException ie){
            throw new GeneralException("Internal Server Exception", ie);
        }

        return data;
    }

}

package com.pdfprocessor.services;


import com.pdfprocessor.core.Splitter;
import com.pdfprocessor.exceptions.FileNotSupportedException;
import com.pdfprocessor.exceptions.GeneralException;
import com.pdfprocessor.utils.Filename;
import com.pdfprocessor.utils.Zipper;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;


/**
 * This Service class stores the multipart file in the temp folder then calls the splitter method from
 * pdfbox and return a list of PdDocuments which then are zipped with the help of Zipper utility class.
 * then finally returns the path of the zipped file.
 *
 * closeEverything method is a private method that is called everytime before throwing an runtime exception so
 * that all the resources are closed.
 */
@Service
public class PdfSplitServiceImpl implements PdfSplitService {

    @Autowired
    private Zipper zipper;

    @Autowired
    private FileServiceImpl fileService;


    @Override
    public Path split(MultipartFile multipartFile, String filename, int pages){

        Path inputFilePath = fileService.store(multipartFile, Filename.pdfFilename(filename));;


        PDDocument pdDocument = new PDDocument();
        List<PDDocument> pdDocuments;
        try{
            pdDocument = PDDocument.load(inputFilePath.toFile());
            pdDocuments = Splitter.split(pdDocument, pages);
        }catch (IOException ioException){
            closeEverything(pdDocument, inputFilePath);
            throw new FileNotSupportedException("File is corrupted or different than PDF/A-1b.");
        }

        Path outputFilePath = fileService.createFile(Filename.zipFilename(filename));

        try {
            zipper.zip(pdDocuments, filename, outputFilePath);
        } catch (IOException ioException) {
            throw new GeneralException("Internal Server Error", ioException);
        }

        closeEverything(pdDocument, inputFilePath);
        return outputFilePath;

    }


    private void closeEverything(PDDocument pdDocument, Path path) {

        try{
            if (pdDocument!=null){
                pdDocument.close();
            }
            if (path!=null){
                fileService.delete(path.getFileName().toString());
            }
        }catch (IOException ioException){
            throw new GeneralException("Internal Server Error", ioException);
        }

    }
}
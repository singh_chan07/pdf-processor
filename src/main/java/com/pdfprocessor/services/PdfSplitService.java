package com.pdfprocessor.services;


import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface PdfSplitService {

    Path split(MultipartFile file, String filename, int pages);
}

package com.pdfprocessor.services;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface FileService {
    Path store(MultipartFile multipartFile, String filename);
    Path createFile();
    Path createFile(String filename);
    void delete(String fileNumber);
    byte [] fetch(String fileNumber);
    String getFilename(String fileNumber);
}

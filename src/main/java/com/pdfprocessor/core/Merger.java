package com.pdfprocessor.core;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDMetadata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class Merger {

    public static OutputStream merge(List<InputStream> inputStreams,
                                     PDDocumentInformation pdDocumentInformation,
                                     PDMetadata pdMetadata, OutputStream outputStream) throws IOException {
        PDFMergerUtility pdfMerger = new PDFMergerUtility();


        pdfMerger.addSources(inputStreams);
        pdfMerger.setDestinationStream(outputStream);
        pdfMerger.setDestinationDocumentInformation(pdDocumentInformation);
        pdfMerger.setDestinationMetadata(pdMetadata);
        pdfMerger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());


        return outputStream;

    }
}

package com.pdfprocessor.core;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.IOException;
import java.util.List;

public class Splitter {

    public static List<PDDocument> split(PDDocument pdDocument, int pages) throws IOException {
        org.apache.pdfbox.multipdf.Splitter splitter = new org.apache.pdfbox.multipdf.Splitter();
        splitter.setSplitAtPage(pages);
        return splitter.split(pdDocument);
    }
}

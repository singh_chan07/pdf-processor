package com.pdfprocessor.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@RestControllerAdvice
public class PdfExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(FileNotSupportedException.class)
    public ResponseEntity<BaseError> handleFileException(FileNotSupportedException fileException, WebRequest request){
        BaseError error = new BaseError(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                fileException.getMessage(),
                request.getDescription(false)
        );
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(GeneralException.class)
    public ResponseEntity<BaseError> handleBaseException(GeneralException generalException, WebRequest request){
        BaseError error = new BaseError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new Date(),
                generalException.getMessage(),
                request.getDescription(false)
        );
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }






}

package com.pdfprocessor.exceptions;

import java.util.Date;


public class BaseError {

    private Integer httpStatus;

    private Date date;

    private String message;

    private String path;


    public BaseError(int value, Date date, String message, String description) {
        this.httpStatus = value;
        this.date = date;
        this.message = message;
        this.path = description;
    }

    public BaseError() {
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

package com.pdfprocessor.exceptions;

public class FileNotSupportedException extends RuntimeException{
    public FileNotSupportedException() {
    }

    public FileNotSupportedException(String message) {
        super(message);
    }

    public FileNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileNotSupportedException(Throwable cause) {
        super(cause);
    }

    public FileNotSupportedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

package com.pdfprocessor.dtos;


public class Signature {

    private String title;

    private String creator;

    private String subject;

    public Signature(String title, String creator, String subject) {
        this.title = title;
        this.creator = creator;
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}

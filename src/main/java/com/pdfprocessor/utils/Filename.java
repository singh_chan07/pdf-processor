package com.pdfprocessor.utils;

public class Filename {
    private static final String zip = ".zip";
    private static final String pdf = ".pdf";

    public static String zipFilename(String filename){
        return filename+zip;
    }
    public static String pdfFilename(String filename){
        return filename+pdf;
    }
}

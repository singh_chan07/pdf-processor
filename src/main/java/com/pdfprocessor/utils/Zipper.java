package com.pdfprocessor.utils;

import com.pdfprocessor.services.FileServiceImpl;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
@Lazy
public class Zipper {


    @Autowired
    private FileServiceImpl fileService;


    public  void zip(List<PDDocument> pdDocumentList, String filename, Path path) throws IOException {


        FileOutputStream fileOutputStream = new FileOutputStream(path.toFile());
        ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);


        int count = 1;
        for(PDDocument document: pdDocumentList){
            File file = fileService.createFile().toFile();
            document.save(file);
            FileInputStream fileInputStream = new FileInputStream(file);
            ZipEntry zipEntry = new ZipEntry(filename+count+".pdf");
            zipOutputStream.putNextEntry(zipEntry);

            byte [] bytes = new byte[1<<10];
            int length;
            while((length=fileInputStream.read(bytes))>=0){
                zipOutputStream.write(bytes, 0, length);
            }

            fileInputStream.close();
            file.delete();
            count++;
            document.close();
        }
        zipOutputStream.close();
        fileOutputStream.close();



    }
}

package com.pdfprocessor.utils;

import com.pdfprocessor.configurations.DirectoryConfigProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Random;

@Component
@EnableConfigurationProperties(DirectoryConfigProperty.class)
public class PathRegistry {

    private final Integer BOUND = Integer.MAX_VALUE;

    @Autowired
    private DirectoryConfigProperty directory;

    
    private HashMap<String, String> registry = new HashMap<>();


    public Path generatePath() {
        return generatePath(null);
    }

    public Path generatePath(String filename){
        Random random = new Random();

        int fileNumber = random.nextInt(BOUND);
        if(registry.containsKey(Integer.toString(fileNumber))){
            while (registry.containsKey(Integer.toString(fileNumber))){
                fileNumber = random.nextInt(BOUND);
            }
        }

        Path path = Paths.get(directory.getDirectory(), Integer.toHexString(fileNumber));
        registry.put(path.toString(), filename);

        return path;

    }

    public String getFilename(String fileNumber){
        return registry.get(Paths.get(directory.getDirectory(), fileNumber).toString());
    }



    public void deletePath(Path path) {

        registry.remove(path.toString());
    }
}
